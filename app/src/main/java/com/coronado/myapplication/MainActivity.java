package com.coronado.myapplication;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rvElementos;
    Adaptador miAdaptador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Tenemos el RV
        rvElementos=findViewById(R.id.rvElementos);
        rvElementos.setLayoutManager(new LinearLayoutManager(this));

        //Definir datos. Pueden venir de muchos sitios
        getElementos();


        //Datos definidos aquí:
        List<String> misDatosLocales= new ArrayList<>();
        misDatosLocales.add("Paulo");
        misDatosLocales.add("Pepe");
        misDatosLocales.add("María");
        misDatosLocales.add("Alba");
        misDatosLocales.add("Jesús");

        //Tomarlos desde una base de datos local/remota ROOM

        //Tomar desde los recursos tipos StringArray
        List<String> misDatosString= Arrays.asList(getResources().getStringArray(R.array.nombres));

        //Crear un objeto de la clase Adaptador
        miAdaptador=new Adaptador(misDatosString);
        rvElementos.setAdapter(miAdaptador);



    }

    public void getElementos(){

        String miUrl="https://www.balldontlie.io/api/v1/teams"; //Retorna un JSON

        RequestQueue miPila= Volley.newRequestQueue(this);

        //Realizar la petición

        JsonObjectRequest miPeticion;

        //Inicializar el objeto como una petición

        miPeticion=new JsonObjectRequest(Request.Method.GET, miUrl, null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Obtener los datos que nos retornó la API
                        try {
                            //Cargar el  string full_name de cada uno de los equipos
                            JSONArray miArreglo = response.getJSONArray("data");
                            List<String> elementos = new ArrayList<>();

                            for (int i = 0; i < miArreglo.length(); i++) {
                                JSONObject miElemento = miArreglo.getJSONObject(i);
                                elementos.add(miElemento.getString("full_name"));
                            }

                            //Mostrar los resultados en el recyclerView

                            RecyclerView rv = findViewById(R.id.rvElementos);
                            miAdaptador = new Adaptador(elementos);
                            rv.setAdapter(miAdaptador);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Hubo un error");
                    }
        });
        miPila.add(miPeticion);
    }
}